import sbt._
import Keys._
import play.Project._
import sbtscalaxb.Plugin._
import ScalaxbKeys._

object ApplicationBuild extends Build {

  val appName         = "moontest"
  val appVersion      = "1.0"

  val appDependencies = Seq(
    "net.databinder.dispatch" % "dispatch-core_2.10" % "0.11.2"
  )

  lazy val buildSettings = Seq(
    scalaVersion := "2.10.4",
    scalacOptions ++= Seq("-Xlint", "-deprecation", "-feature", "-language:postfixOps")
  )

  lazy val customXbSettings = Seq(
    packageName in scalaxb in Compile := "org.hiba.term",
    sourceGenerators in Compile <+= scalaxb in Compile,
    xsdSource in scalaxb in Compile := file("conf"),
    wsdlSource in scalaxb in Compile := file("conf")
  )

  lazy val allSettings = buildSettings ++ scalaxbSettings ++ customXbSettings

  val main = play.Project(appName, appVersion, appDependencies).settings(allSettings: _*)
}

