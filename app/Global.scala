import play.api._
import play.api.mvc._
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.libs.ws._
import scala.concurrent.Future
import scala.util.Try
import java.util.concurrent.Executors
import play.api.libs.concurrent.Execution.Implicits.defaultContext

object Global extends WithFilters(Stealth)

object Stealth extends Filter {

  val Unauth = Future.successful(Results.Unauthorized.withHeaders("WWW-Authenticate" -> "Basic realm=omnia"))
  def apply(next: (RequestHeader) => Future[SimpleResult])(req: RequestHeader) = unlessLocal(next, req) {
    val decoded = for (authString <- req.headers.get("Authorization")) yield decodeBasicAuth(authString)

    decoded.map { case (usr, pass) =>
      if (usr == "omnia" && pass == "0mn14") {
        next(req)
      } else {
        Unauth
      }
    }.getOrElse(Unauth)
  }

  def decodeBasicAuth(auth: String) = Try {
    val baStr = auth.replaceFirst("Basic ", "")
    val Array(user, pass) = new String(new sun.misc.BASE64Decoder().decodeBuffer(baStr), "UTF-8").split(":")
    (user, pass)
  } getOrElse(("",""))

  private def unlessLocal(next: (RequestHeader) => Future[SimpleResult], req: RequestHeader)(block: => Future[SimpleResult]) = {
    if (req.domain == "localhost" || req.method.equalsIgnoreCase("head")) next(req) else block
  }
}

