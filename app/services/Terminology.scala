package services

import org.hiba.term._
import scalaxb.Soap11ClientsAsync
import scalaxb.HttpClientsAsync
import scala.concurrent.Future
import play.api.Logger

/*
 * Métodos:
 *
 *  obtenerAltaEfectoAdversoXHallazgoAgenteCausalYSeveridad
 *  obtenerAtributosDeDescription
 *  obtenerAuditoriaTermino
 *  obtenerClasificador
 *  obtenerClasificadorUnico
 *  obtenerCodigoAltaEfectoAdversoXAgenteCausal
 *  obtenerCodigoAltaEfectoAdversoXAgenteCausalYSeveridad
 *  obtenerCodigoAltaEfectoAdversoXHallazgoYAgenteCausal
 *  obtenerCodigoAntecedenteFamiliarPorDescriptionId
 *  obtenerCodigoSNMapeo
 *  obtenerCodigoTesauro
 *  obtenerComposicionFarmacoXDescId
 *  obtenerComposicionGenericoAmbulatorio
 *  obtenerComposicionPresentacionAmbulatorio
 *  obtenerConceptXDescID
 *  obtenerDataDescriptionId
 *  obtenerFamiliares
 *  obtenerGenericoXPresentacionComercial
 *  obtenerGenericosPorTxtYSubset
 *  obtenerListaDeSubsets
 *  obtenerMapSetsCorrientes
 *  obtenerMedicamentoBasicoPorMedicamentoClinico
 *  obtenerMedicamentoClinicoPorMedicamentoBasico
 *  obtenerMembers
 *  obtenerMembersConceptoSubset
 *  obtenerModeladoHIBADescription
 *  obtenerOfertaSimilitud
 *  obtenerOfertaSimilitudIndependienteSubsetConOfeSubset
 *  obtenerOfertaSimilitudXFrecuenciaUso
 *  obtenerOfertaTextos
 *  obtenerOfertaTextosConCaducidad
 *  obtenerOfertaTextosIndependienteSubset
 *  obtenerOfertaTextosIndependienteSubsetConCaducidadYOfertaSubset
 *  obtenerOfertaTextosIndependienteSubsetConOfeSubset
 *  obtenerPresentacionesComercialesXGenerico
 *  obtenerPrompting
 *  obtenerSubsetsXDescId
 */
object Terminology {
  val service = (new TerminologiaWSPortBindings with Soap11ClientsAsync with DispatchClientWithTimeout ).service
  val Problems = Some(601000999132L)
  val Code = Some(41701000999134L)
  val Procedures = Some(601000999132L)
  val Substance = Some(591000999139L)

  val Amp = Some(31621000999133L)
  val Ampp = Some(35001000999130L)
  val Vmp = Some(31021000999137L)
  val Vtm = Some(31011000999130L)

  def find(term: String): Future[ObtenerOfertaTextosResponse] = {
    service.obtenerOfertaTextos(Some(term), Problems, Code)
  }

  def findGenericDrug(term: String): Future[ObtenerGenericosPorTxtYSubsetResponse] = {
    service.obtenerGenericosPorTxtYSubset(Some(term), Vmp, Code) recover {
      case _ => ObtenerGenericosPorTxtYSubsetResponse(HeaderGeneric)
    }
  }

  def findComercialDrug(term: String): Future[ObtenerGenericosPorTxtYSubsetResponse] = {
    service.obtenerGenericosPorTxtYSubset(Some(term), Ampp, Code) recover {
      case _ => ObtenerGenericosPorTxtYSubsetResponse(HeaderGeneric)
    }
  }

  def findBaseDrug(term: String): Future[ObtenerGenericosPorTxtYSubsetResponse] = {
    service.obtenerGenericosPorTxtYSubset(Some(term), Vtm, Code) recover {
      case _ => ObtenerGenericosPorTxtYSubsetResponse(HeaderGeneric)
    }
  }

  def findGenericFromComercialId(descId: Long): Future[ObtenerGenericoXPresentacionComercialResponse] = {
    service.obtenerGenericoXPresentacionComercial(Some(descId), Code)
  }

  def findGenericComposition(id: Long): Future[ObtenerComposicionGenericoAmbulatorioResponse] = {
    service.obtenerComposicionGenericoAmbulatorio(Some(id), Code)
  }

  def findComercialComposition(id: Long): Future[ObtenerComposicionPresentacionAmbulatorioResponse] = {
    service.obtenerComposicionPresentacionAmbulatorio(Some(id), Code)
  }

  def findSubstance(term: String): Future[ObtenerOfertaTextosResponse] = {
    service.obtenerOfertaTextos(Some(term), Substance, Code)
  }

  lazy val Header = OfertaTextoCabecera(dominios = Some("dominios"),
    entrada = Some("entrada"),
    explicacion = Some("explicacion"),
    idDescripcion = Some("id_descripcion"),
    idDescripcionPreferido = Some("id_descripcion_preferido"),
    legible = Some("legible"),
    multiplicidad = Some("multiplicidad"),
    ofertaTextoDetalle = Seq(Some(Details)),
    refinacionObligatoria = Some("refinacion_obligatoria"),
    textoPreferido = Some("texto_preferido"),
    titulo = Some("titulo"))

  lazy val HeaderGeneric = Member(descId = Some(1), descripcion = Some("descripcion"))

  lazy val Details = OfertaTextoDetalle(conceptId = Some(1), descriptionId = Some(1), texto = Some("texto"),
    textoOrigenSimilitud = Some("texto_origen_similitud"))
}

/*
 * Need to override this since the default timeout for
 * AsyncHttpClientConfig is ~30 seconds.
 */
trait DispatchClientWithTimeout extends HttpClientsAsync {
  import dispatch._
  import com.ning.http.client.AsyncHttpClientConfig

  val http = Http.configure { config =>
    config.setRequestTimeoutInMs(6000)
  }

  def httpClient = new HttpClient {
    def request(in: String, address: java.net.URI, headers: Map[String, String]): concurrent.Future[String] = {
      val req = url(address.toString).setBodyEncoding("UTF-8") <:< headers << in
      http(req > as.String)
    }
  }
}