package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.Logger
import services.Terminology
import org.hiba.term._
import scala.concurrent.Future

object Application extends Controller {
  implicit val detailWrites = Json.writes[OfertaTextoDetalle]
  implicit val headerWrites = Json.writes[OfertaTextoCabecera]
  implicit val textosIndepSubsetResponse = Json.writes[ObtenerOfertaTextosIndependienteSubsetResponse]
  implicit val textosResponse = Json.writes[ObtenerOfertaTextosResponse]
  implicit val memberWrites = Json.writes[Member]
  implicit val composicionDetalleWrites = Json.writes[ComposicionGenericoAmbulatorioDetalle]
  implicit val composicionCabeceraWrites = Json.writes[ComposicionGenericoAmbulatorioCabecera]
  implicit val composicionWrites = Json.writes[ObtenerComposicionGenericoAmbulatorioResponse]
  implicit val compComercialCabeceraWrites = Json.writes[ComposicionPresentacionAmbulatorioCabecera]
  implicit val compoComercialWrites = Json.writes[ObtenerComposicionPresentacionAmbulatorioResponse]

  implicit val genericWrites = new Writes[ObtenerGenericosPorTxtYSubsetResponse] {
    def writes(o: ObtenerGenericosPorTxtYSubsetResponse) = {
      val result = ObtenerGenericosPorTxtYSubsetResponse.unapplySeq(o).getOrElse(Seq())
      Json.toJson(result.map(m => (m.descId.getOrElse(-1L).toString, m.descripcion.getOrElse(""))).toMap)
    }
  }

  implicit val comercialWrites = new Writes[ObtenerGenericoXPresentacionComercialResponse] {
    def writes(o: ObtenerGenericoXPresentacionComercialResponse) = {
      val result = ObtenerGenericoXPresentacionComercialResponse.unapplySeq(o).getOrElse(Seq())
      Json.toJson(result.map(m => (m.descId.getOrElse(-1L).toString, m.descripcion.getOrElse(""))).toMap)
    }
  }

  def search(term: String, kind: String) = Action.async {
    val start = System.currentTimeMillis

    getResponse(term, kind).map { value =>
      val json = value.asInstanceOf[JsValue]
      val took = System.currentTimeMillis - start
      Ok(json)
    }
  }

  private def getResponse(term: String, kind: String): Future[JsValue] = kind match {
    case "indep" => Terminology.find(term) map (it => Json.toJson(it))
    case "generic" => Terminology.findGenericDrug(term) map (it => Json.toJson(it))
    case "comercial" => Terminology.findComercialDrug(term) map (it => Json.toJson(it))
    case "base" => Terminology.findBaseDrug(term) map (it => Json.toJson(it))
    case "comercialToGeneric" => Terminology.findGenericFromComercialId(term.toLong) map (it => Json.toJson(it))
    case "compositionGeneric" => Terminology.findGenericComposition(term.toLong) map (it => Json.toJson(it))
    case "compositionComercial" => Terminology.findComercialComposition(term.toLong) map (it => Json.toJson(it))
    case "subs" => Terminology.findSubstance(term) map (it => Json.toJson(it))
    case _ => Future.successful(Json.obj())
  }

  def index = Action {
    Ok(views.html.index())
  }
}